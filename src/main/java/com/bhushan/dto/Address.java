package com.bhushan.dto;
/**
* @author Author E-mail:
 * @version Creation time: 01-Jul-2021 5:19:31 pm
 * Class Description
*/
public class Address {

	private String street;
	private String suite;
	private String zipcode;
	private String city;
	private Geo geo;
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getSuite() {
		return suite;
	}
	public void setSuite(String suite) {
		this.suite = suite;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Geo getGeo() {
		return geo;
	}
	public void setGeo(Geo geo) {
		this.geo = geo;
	}
	
	
}
