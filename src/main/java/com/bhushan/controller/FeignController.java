package com.bhushan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.dto.Employee;
import com.bhushan.dto.Response;
import com.bhushan.openfeign.FeignClients;

@RestController
public class FeignController {

	@Autowired
	private FeignClients feignClients;

	@GetMapping("/getEmployees")
	public Response getemployees() {
		Response response = new Response();
		List<Employee> employees = feignClients.getEmployees();
		if (employees.size() > 0) {
			response.setStatus("200");
			response.setMessage("The Data has been Retrieved");
			response.setEmployee(employees);
		} else {
			response.setStatus("500");
			response.setMessage("Something went wrong");
		}
		return response;
	}
}
