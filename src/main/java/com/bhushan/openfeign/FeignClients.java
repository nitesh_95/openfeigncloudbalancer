package com.bhushan.openfeign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.bhushan.dto.Employee;

@FeignClient(url="https://jsonplaceholder.typicode.com",name = "USER-CLIENT")
public interface FeignClients {

	@GetMapping("/users")
	public List<Employee> getEmployees();
}
